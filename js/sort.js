function sortUnorderedList(ul,colm) {
  if(typeof ul == "string")
    ul = document.getElementById(ul);

  var lis = ul.getElementsByClassName(colm);
  var vals = [];

  for(var i = 0, l = lis.length; i < l; i++)
    vals.push(lis[i].innerHTML);

  vals.sort();

  for(var i = 0, l = lis.length; i < l; i++)
    lis[i].innerHTML = vals[i];
}

function sortByArtist(a, b) {
	var aText=$(a).find('.artist').text();
	var bText=$(b).find('.artist').text();
    return aText==bText?0:aText<bText?-1:1;
}

function sortByTitle(a, b) {
	var aText=$(a).find('.songtitle').text();
	var bText=$(b).find('.songtitle').text();
    return aText==bText?0:aText<bText?-1:1;
}

function sortByAlbum(a, b) {
	var aText=$(a).find('.album').text();
	var bText=$(b).find('.album').text();
    return aText==bText?0:aText<bText?-1:1;
}

function sortTheTable(colm){
    $(function() {
        elems=$.makeArray($('tr:has(.'+colm+')'));
        if(colm == "artist")
        	elems.sort(sortByArtist);
        else if(colm == "songtitle")
        	elems.sort(sortByTitle);
        else if(colm == "album")
        	elems.sort(sortByAlbum);
        $('#songs').append(elems);
    });
}

window.onload = function() {
  document.getElementById("artistsort").onclick = function(){
    sortTheTable("artist");
  }
  document.getElementById("titlesort").onclick = function(){
  	sortTheTable("songtitle");
  }
  document.getElementById("albumsort").onclick = function(){
  	sortTheTable("album");
  }
}